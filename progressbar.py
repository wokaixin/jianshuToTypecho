import sys
import time
 
class progressbar(object):
 
  def __init__(self, finalcount,title="进度", block_char='▋'):
    if title==None:
        title="进度信息"
    if block_char==None:
        block_char='▋'
    self.startTime=time.perf_counter()
    self.finalcount = finalcount
    self.blockcount = 0
    self.title=title
    self.block = block_char
    self.f = sys.stdout
    if not self.finalcount:
      return
    self.f.write('\n------------------ % '+self.title+' -------------------1\n')
    self.f.write(' 1 2 3 4 5 6 7 8 9 0\n')
    self.f.write('----0----0----0----0----0----0----0----0----0----0\n')
 
  def progress(self, count):
    during = time.perf_counter() - self.startTime
    count = min(count, self.finalcount)
    if self.finalcount:
      percentcomplete = int(round(100.0 * count / self.finalcount))
      if percentcomplete < 1:
        percentcomplete = 1
    else:
      percentcomplete = 100
    blockcount = int(percentcomplete // 2)
    if blockcount <= self.blockcount:
      return
    scale = 100
    progress = self.block * percentcomplete
    point = "." * (scale - percentcomplete)
    during = time.perf_counter() - self.startTime
    print("\r{:^3.0f}%【{}->{}】{:.2f}s".format(percentcomplete, progress, point, during), end="")
    self.f.flush()
    self.blockcount = blockcount
    if percentcomplete == 100:
      self.f.write("\n")
 
def test():
  from time import sleep
  pb = progressbar(110)
  for count in range(0, 111):
    # print(count)
    pb.progress(count)
    sleep(0.002)
# test()
