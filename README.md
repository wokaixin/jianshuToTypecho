# 简书markdown文档导入typecho博客数据库脚本

#### 介绍
#简书MD文件导入Typecho数据库
作者：yichen    email：2782268022@qq.com

#### 软件架构
软件架构说明
python 3.8.6


#### 使用说明

#简书MD文件导入Typecho数据库
作者：yichen    email：2782268022@qq.com
# 使用命令参数
帮助查看
```
python -h
```
# 路径
--path  file
# 后缀
--filetype   .md
# 数据库链接
--dbhost localhost
# 数据库用户名
--dbuser root
# 数据库密码
--dbpasswd 123456
# 数据库名
--dbname typecho
# 数据库字符集
--dbcharset utf8mb4

# 简化命令
```
-dh localhost -dp 3306 -du root -dpwd 123456 -dn typecho -dc utf8mb4   -p file -f .m
```
# 完整命令
```
--dbcharset utf8mb4  --dbhost localhost --dbname typecho  --dbpasswd 123456 --dbport=3306 --dbuser root  --filesuffix .md  --path file 
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
